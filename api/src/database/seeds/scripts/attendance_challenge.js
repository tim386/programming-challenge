/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.seed = async function(knex) {
    console.log('Re-Seeding the DB...');
    // Deletes ALL existing entries
    await knex('attendance').truncate()

    const attendance = [
        { 'attendance_date' : '2023-03-02', periods_missed: 450 },
        { 'attendance_date' : '2023-03-03', periods_missed: 450 },
        { 'attendance_date' : '2023-03-04', periods_missed: 450 },
        { 'attendance_date' : '2023-03-05', periods_missed: 526 },
        { 'attendance_date' : '2023-03-06', periods_missed: 825 },
        { 'attendance_date' : '2023-03-07', periods_missed: 896 },
        { 'attendance_date' : '2023-03-08', periods_missed: 450 },
        { 'attendance_date' : '2023-03-09', periods_missed: 450 },
        { 'attendance_date' : '2023-03-10', periods_missed: 450 },
        { 'attendance_date' : '2023-03-11', periods_missed: 450 },
        { 'attendance_date' : '2023-03-12', periods_missed: 685 },
        { 'attendance_date' : '2023-03-13', periods_missed: 875 },
        { 'attendance_date' : '2023-03-14', periods_missed: 906 },
        { 'attendance_date' : '2023-03-15', periods_missed: 501 },
        { 'attendance_date' : '2023-03-16', periods_missed: 650 },
        { 'attendance_date' : '2023-03-17', periods_missed: 795 },
        { 'attendance_date' : '2023-03-18', periods_missed: 450 },
        { 'attendance_date' : '2023-03-19', periods_missed: 450 },
        { 'attendance_date' : '2023-03-20', periods_missed: 450 },
        { 'attendance_date' : '2023-03-21', periods_missed: 450 },
        { 'attendance_date' : '2023-03-22', periods_missed: 450 },
        { 'attendance_date' : '2023-03-23', periods_missed: 506 },
        { 'attendance_date' : '2023-03-24', periods_missed: 557 },
        { 'attendance_date' : '2023-03-25', periods_missed: 568 },
        { 'attendance_date' : '2023-03-26', periods_missed: 450 },
        { 'attendance_date' : '2023-03-27', periods_missed: 450 },
        { 'attendance_date' : '2023-03-28', periods_missed: 450 },
        { 'attendance_date' : '2023-03-29', periods_missed: 450 },
        { 'attendance_date' : '2023-03-30', periods_missed: 450 },
        { 'attendance_date' : '2023-03-31', periods_missed: 450 },
        { 'attendance_date' : '2023-04-01', periods_missed: 450 },
        { 'attendance_date' : '2023-04-02', periods_missed: 450 },
        { 'attendance_date' : '2023-04-03', periods_missed: 450 },
        { 'attendance_date' : '2023-04-04', periods_missed: 450 },
        { 'attendance_date' : '2023-04-05', periods_missed: 450 },
        { 'attendance_date' : '2023-04-06', periods_missed: 450 },
        { 'attendance_date' : '2023-04-07', periods_missed: 450 },
        { 'attendance_date' : '2023-04-08', periods_missed: 545 },
        { 'attendance_date' : '2023-04-09', periods_missed: 654 },
        { 'attendance_date' : '2023-04-10', periods_missed: 859 },
        { 'attendance_date' : '2023-04-11', periods_missed: 450 },
        { 'attendance_date' : '2023-04-12', periods_missed: 450 },
        { 'attendance_date' : '2023-04-13', periods_missed: 450 },
        { 'attendance_date' : '2023-04-14', periods_missed: 450 },
        { 'attendance_date' : '2023-04-15', periods_missed: 450 },
        { 'attendance_date' : '2023-04-16', periods_missed: 450 },
        { 'attendance_date' : '2023-04-17', periods_missed: 450 },
        { 'attendance_date' : '2023-04-18', periods_missed: 450 },
        { 'attendance_date' : '2023-04-19', periods_missed: 450 },
        { 'attendance_date' : '2023-04-20', periods_missed: 450 },
        { 'attendance_date' : '2023-04-21', periods_missed: 450 },
        { 'attendance_date' : '2023-04-22', periods_missed: 450 },
        { 'attendance_date' : '2023-04-23', periods_missed: 450 },
        { 'attendance_date' : '2023-04-24', periods_missed: 450 },
        { 'attendance_date' : '2023-04-25', periods_missed: 450 },
        { 'attendance_date' : '2023-04-26', periods_missed: 450 },
        { 'attendance_date' : '2023-04-27', periods_missed: 450 },
        { 'attendance_date' : '2023-04-28', periods_missed: 450 },
        { 'attendance_date' : '2023-04-29', periods_missed: 450 },
        { 'attendance_date' : '2023-04-30', periods_missed: 450 },
        { 'attendance_date' : '2023-05-01', periods_missed: 450 },
        { 'attendance_date' : '2023-05-02', periods_missed: 450 },
        { 'attendance_date' : '2023-05-03', periods_missed: 450 },
        { 'attendance_date' : '2023-05-04', periods_missed: 450 },
        { 'attendance_date' : '2023-05-05', periods_missed: 450 },
        { 'attendance_date' : '2023-05-06', periods_missed: 450 },
        { 'attendance_date' : '2023-05-07', periods_missed: 450 },
        { 'attendance_date' : '2023-05-08', periods_missed: 450 },
        { 'attendance_date' : '2023-05-09', periods_missed: 450 },
        { 'attendance_date' : '2023-05-10', periods_missed: 450 },
        { 'attendance_date' : '2023-05-11', periods_missed: 450 },
        { 'attendance_date' : '2023-05-12', periods_missed: 450 },
        { 'attendance_date' : '2023-05-13', periods_missed: 450 },
        { 'attendance_date' : '2023-05-14', periods_missed: 450 },
        { 'attendance_date' : '2023-05-15', periods_missed: 450 },
        { 'attendance_date' : '2023-05-16', periods_missed: 450 },
        { 'attendance_date' : '2023-05-17', periods_missed: 450 },
        { 'attendance_date' : '2023-05-18', periods_missed: 450 },
        { 'attendance_date' : '2023-05-19', periods_missed: 450 },
        { 'attendance_date' : '2023-05-20', periods_missed: 450 },
        { 'attendance_date' : '2023-05-21', periods_missed: 450 },
        { 'attendance_date' : '2023-05-22', periods_missed: 450 },
        { 'attendance_date' : '2023-05-23', periods_missed: 450 },
        { 'attendance_date' : '2023-05-24', periods_missed: 450 },
        { 'attendance_date' : '2023-05-25', periods_missed: 450 },
        { 'attendance_date' : '2023-05-26', periods_missed: 450 },
        { 'attendance_date' : '2023-05-27', periods_missed: 685 },
        { 'attendance_date' : '2023-05-28', periods_missed: 896 },
        { 'attendance_date' : '2023-05-29', periods_missed: 745 },
        { 'attendance_date' : '2023-05-30', periods_missed: 450 },
        { 'attendance_date' : '2023-05-31', periods_missed: 450 },
        { 'attendance_date' : '2023-06-01', periods_missed: 450 },
        { 'attendance_date' : '2023-06-02', periods_missed: 450 },
        { 'attendance_date' : '2023-06-03', periods_missed: 450 },
        { 'attendance_date' : '2023-06-04', periods_missed: 450 },
        { 'attendance_date' : '2023-06-05', periods_missed: 999 },
        { 'attendance_date' : '2023-06-06', periods_missed: 888 },
        { 'attendance_date' : '2023-06-07', periods_missed: 777 },
        { 'attendance_date' : '2023-06-08', periods_missed: 666 },
        { 'attendance_date' : '2023-06-09', periods_missed: 555 }
    ];

    await knex('attendance').insert(attendance);

    console.log('Re-Seed Done...');
};

